<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img id="landingLogo" src="{{asset('img/final_logo.png')}}" alt="Project Bazaar Logo">
                </div>

                <div class="links">
                  <button id="loginButton" type="button" name="button"><a href="#login">Log in</a></button>
                  <button id="projexButton" type="button" name="button"><a href="#blogs">Projex Blog</a></button>
                  <button id="ideasButton" type="button" name="button"><a href="#ideas">Project Ideas</a></button>
                  <button id="samplesButton" type="button" name="button"><a href="#samples">Project Samples</a></button>
                </div>
            </div>
        </div>
    </body>
</html>
