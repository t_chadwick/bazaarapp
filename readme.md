Started Over.

No longer having problem with git cloning.

Removing the extra .gitignore files meant that the repo now has bootstrap/cache and the storage/framework folders. Don't know if this is a good thing but now it works.
This app can now be downloaded and after doing the following:

- $npm install
- $composer install
- create .env file using .env.example
- $php artisan key:generate

It will run.


Now setup SASS.
Can edit app.scss and it will compile to app.css either:

- after running - $npm run dev-  (or - $npm run production-  for minified version)
- Running - $npm run watch - before hand, this will automatically compile after saving app.scss.
